#include "LPC17xx.h"
#include "Type.h"
#include "ExtInt.h"
#include <stdio.h>

void EINT0_IRQHandler(void)
{
	printf("Interupt Received \n");
	LPC_SC->EXTINT = EINT0; /*clear interrupt */
}
uint32_t EINTInit( void )
{
	//LPC_PINCON->PINSEL1 |= (3<<10);
	//LPC_GPIOINT->IO0IntEnF |= (1<<21);
	LPC_PINCON->PINSEL4 &=~ (3 << 20);
	LPC_PINCON->PINSEL4 |= (1 << 20);
	LPC_GPIOINT->IO2IntEnF |= (1 << 10);
	LPC_SC->EXTMODE = EINT0_EDGE;
	LPC_SC->EXTPOLAR = 0;
	NVIC_EnableIRQ(EINT0_IRQn);
	return( TRUE );
}
