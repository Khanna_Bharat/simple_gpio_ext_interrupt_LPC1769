/*
===============================================================================
 Name        : GPIO_EXT_INT.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#include "extint.h"
#endif

#include <cr_section_macros.h>

#include <stdio.h>


int main(void) {

	while(1)
	{
		EINTInit();
	}
    return 0 ;
}
